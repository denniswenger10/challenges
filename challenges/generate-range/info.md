Complete the function that takes two integers (a, b, where a < b) and return an array of all integers between the input parameters, including them.

For example:

```javascript
generateRange(1, 4) //returns [1, 2, 3, 4]
```