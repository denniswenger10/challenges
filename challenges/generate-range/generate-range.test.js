import { generateRange } from './generate-range'

describe('Sample tests', () => {
  it('Should pass sample tests', () => {
    expect(generateRange(1, 4)).toEqual([1, 2, 3, 4])
    expect(generateRange(10, 20)).toEqual([
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
    ])
  })
})
