Create a function called vowelRemover to remove all the lowercase vowels in a given string.

Examples:
```javascript
vowelRemover("codewars") // --> cdwrs
vowelRemover("goodbye")  // --> gdby
```

Don't worry about uppercase vowels.