import { vowelRemover } from './vowel-remover'

describe('Sample tests', () => {
  it('Should pass sample tests', () => {
    expect(vowelRemover('codewars')).toBe('cdwrs')
    expect(vowelRemover('goodbye')).toBe('gdby')
  })
})
