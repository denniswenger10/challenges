describe('Sample tests', () => {
  it('Should pass sample tests', () => {
    expect(nameShuffler('Dennis Wenger')).toBe('Wenger Dennis')
    expect(nameShuffler('Felix Topsholm')).toBe('Topsholm Felix')
  })
})
