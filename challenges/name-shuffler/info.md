Write a function that returns a string in which firstname is swapped with last name.

```javascript
nameShuffler('john McClane') //returns "McClane john"
```